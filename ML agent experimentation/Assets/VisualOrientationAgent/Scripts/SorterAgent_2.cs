using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using Random = UnityEngine.Random;

public class SorterAgent_2 : Agent
{
    private int NumTiles = 4;
    private bool curriculumThreeStart = true;
    Rigidbody m_AgentRb;

    private Vector3 m_StartingPos;

    public int curriculumStart = 0;

    public GameObject m_Area;
    public GameObject obstacleTile;
    public GameObject TilePrefab;
    EnvironmentParameters m_ResetParams;

    private int m_tileToChase;

    private int curriculum = 2;
    private int completedObjectives = 0;

    private List<GameObject> ObstacleList = new List<GameObject>();
    private List<GameObject> TileListRand = new List<GameObject>();



    public override void Initialize()
    {
        // m_Area = transform.parent.gameObject;
        m_ResetParams = Academy.Instance.EnvironmentParameters;
        m_AgentRb = GetComponent<Rigidbody>();
        m_StartingPos = transform.position;

    }


    private Vector3 SampleRandomPos(int bounds){
        Vector3 randPos = new Vector3(m_Area.transform.position.x + Random.Range(-bounds, bounds), 0.5f, m_Area.transform.position.z + Random.Range(-bounds, bounds));
        if (randPos.x < 1 && randPos.x > - 1 && randPos.y < 1 && randPos.y > - 1){
            randPos = SampleRandomPos(bounds);
        }
        return randPos;
    }


    private void CleanUp(){
        // clean up first
        if (ObstacleList.Count > 0){
            for (int i=0; i< ObstacleList.Count; i++){
                Destroy(ObstacleList[i]);   
            }
            ObstacleList = new List<GameObject>();
        }
        if(TileListRand.Count > 0){
            for (int i=0; i< TileListRand.Count; i++){
                Destroy(TileListRand[i]); 
            }
            TileListRand = new List<GameObject>();
        }
    }

    public override void OnEpisodeBegin()
    {
        completedObjectives = 0;
        Debug.Log("New episode");
        m_tileToChase = Random.Range(0, NumTiles);

        curriculum = (int)m_ResetParams.GetWithDefault("obstacles", curriculumStart);

        CleanUp();

        // if (curriculum == 0){
        //     // set one tile as objective
        //     var randPos = SampleRandomPos(13);
        //     TileListRand.Add(Instantiate(TilePrefab, randPos, Quaternion.identity)); 
        // }        
        // // Get to objective with 4 obstacles
        // if (curriculum == 1){
        //     // set one tile as objective
        //     var randPos = SampleRandomPos(13);
        //     TileListRand.Add(Instantiate(TilePrefab, randPos, Quaternion.identity)); 

        //     for (int i=0; i< 4; i++){
        //         randPos = SampleRandomPos(13);
        //         ObstacleList.Add(Instantiate(obstacleTile, randPos, Quaternion.identity)); 
        //     }
        // }
        // // Get 4 objective with 4 obstacles
        // if (curriculum == 2){ 
        //     // add obstacles 
        //     for (int i=0; i< 4; i++){
        //         var randPos = SampleRandomPos(13);
        //         ObstacleList.Add(Instantiate(obstacleTile, randPos, Quaternion.identity)); 
        //     }
        //     // add objectives
        //     for (int i=0; i< 4; i++){
        //         var randPos = SampleRandomPos(13);
        //         TileListRand.Add(Instantiate(TilePrefab, randPos, Quaternion.identity)); 
        //     }
        // }
        // if (curriculum == 3){ 

        //     // make env big
        //     if (curriculumThreeStart){
        //         Debug.Log("Started");
        //         m_Area.transform.localScale += new Vector3(3f, 0f, 3f);
        //         curriculumThreeStart = false;
        //     }
        //     // add obstacles 
        //     for (int i=0; i< 14; i++){
        //         var randPos = SampleRandomPos(45);
        //         ObstacleList.Add(Instantiate(obstacleTile, randPos, Quaternion.identity)); 
        //     }
        //     // add objectives
        //     for (int i=0; i< 14; i++){
        //         var randPos = SampleRandomPos(45);
        //         TileListRand.Add(Instantiate(TilePrefab, randPos, Quaternion.identity)); 
        //     }
        // }
        

        transform.position = m_StartingPos;
        m_AgentRb.velocity = Vector3.zero;
        m_AgentRb.angularVelocity = Vector3.zero;
    }


    public override void CollectObservations(VectorSensor sensor)
    {
        // agent's direction
        sensor.AddObservation(transform.forward.x);
        sensor.AddObservation(transform.forward.z);
    }

    private void OnCollisionEnter(Collision col)
    {
        // if collision with a Cube Object, return
        if (col.gameObject.CompareTag("tile") )
        {
            Debug.Log("crashed objective!");

            // for advanced curriculi
            if (curriculum == 2){
                AddReward(0.25f);
                Destroy(col.gameObject);
                completedObjectives += 1;
                if (completedObjectives >= 4){
                    EndEpisode();
                }
            }
            else if (curriculum == 3){
                AddReward(0.1f);
                Destroy(col.gameObject);
                completedObjectives += 1;
                if (completedObjectives >= 14){
                    EndEpisode();
                }
            }
            else{
                AddReward(1);

                // The Agent Succeeded
                EndEpisode();
            }


        }
        else if (col.gameObject.CompareTag("obstacle"))
        {
            Debug.Log("crashed obstacle!");
            // The Agent Succeeded
            AddReward(-1);
            EndEpisode();
        }
        else
        {
            return;
        }
    }


    /// <summary>
    /// Moves the agent according to the selected action.
    /// </summary>
    public void MoveAgent(ActionSegment<int> act)
    {
        var dirToGo = Vector3.zero;
        var rotateDir = Vector3.zero;

        var forwardAxis = act[0];
        var rightAxis = act[1];
        var rotateAxis = act[2];

        switch (forwardAxis)
        {
            case 1:
                dirToGo = transform.forward * 1f;
                break;
            case 2:
                dirToGo = transform.forward * -1f;
                break;
        }

        switch (rightAxis)
        {
            case 1:
                dirToGo = transform.right * 1f;
                break;
            case 2:
                dirToGo = transform.right * -1f;
                break;
        }

        switch (rotateAxis)
        {
            case 1:
                rotateDir = transform.up * -1f;
                break;
            case 2:
                rotateDir = transform.up * 1f;
                break;
        }

        transform.Rotate(rotateDir, Time.deltaTime * 200f);
        m_AgentRb.AddForce(dirToGo * 2, ForceMode.VelocityChange);
    }

    /// <summary>
    /// Called every step of the engine. Here the agent takes an action.
    /// </summary>
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Move the agent using the action.
        MoveAgent(actionBuffers.DiscreteActions);

        // Penalty given each step to encourage agent to finish task quickly.
        AddReward(-1f / MaxStep);
    }
}
