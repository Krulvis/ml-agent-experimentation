using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentController : MonoBehaviour
{

    public List<GameObject> TargetObjects;
    public GameObject TargetCubePrefab;
    public GameObject TargetCubePrefabPlus;
    public GameObject TargetCubePrefabMin;
    // Start is called before the first frame update
    void Start()
    {   

    }

    public void GenerateEpisode(){
        SetTarget();
        SetTarget();
        SetTarget();
        SetTarget();
        SetTarget();
        SetTarget();
        SetTarget();
        SetTarget();
        SetTarget();
        SetTarget();
        Debug.Log("generagting epidose");
    }


    private Vector3 SampleRandomPos(){
        int bounds = 20;

        // Vector3 cur = gameObject.transform.position;

        // select whether on xmin, xmax, zmin, zmax
        int side = Random.Range(0,4);
        Vector3 randPos= gameObject.transform.position;
        switch (side)
        {
            case 0: // xmin
                randPos += new Vector3(-30,Random.Range(0, bounds),Random.Range(-bounds, bounds));
                break;
            case 1: // xmax
                randPos += new Vector3(30,Random.Range(0, bounds),Random.Range(-bounds, bounds));
                break;
            case 2: // zmin
                randPos += new Vector3(Random.Range(-bounds, bounds),Random.Range(0, bounds),-30);
                break;
            case 3: // zmax
                randPos += new Vector3(Random.Range(-bounds, bounds),Random.Range(0, bounds),30);
                break;
        }

        return randPos;
    }


    public void SetTarget(){

        Vector3 randPos = SampleRandomPos();
        GameObject TargetPrefabChoice = new List<GameObject>{TargetCubePrefab, TargetCubePrefabMin, TargetCubePrefabPlus}[Random.Range(0,3)];
        TargetObjects.Add(Instantiate(TargetPrefabChoice, randPos, Quaternion.identity));
    }

    public void ResetEpisode(){
        // clean up first
        if (TargetObjects.Count > 0){
            for (int i=0; i< TargetObjects.Count; i++){
                if (TargetObjects[i] != null){
                    Destroy(TargetObjects[i]);   
                }
            }
            TargetObjects = new List<GameObject>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
