using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    [SerializeField] private Material HighlightMaterial;
    [SerializeField] private Camera AgentCamera;
    public int _pressable = 0;
    private Material _historicalMaterial;
    private Transform _selection;

    // Update is called once per frame
    void Update()
    {
        if (_selection != null){
            if (_selection.tag == "Selectable" ||_selection.tag == "SelectableMin" ||_selection.tag == "SelectablePlus"){
                var selectionRenderer = _selection.GetComponent<Renderer>();
                selectionRenderer.material = _historicalMaterial;
                _selection = null;
            }
        }

        var ray = AgentCamera.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, AgentCamera.nearClipPlane));

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)){
            
            var selection = hit.transform;
            string tag = selection.tag;

            // check if one of the tags
            if (tag == "Selectable" || tag == "SelectablePlus" || tag == "SelectableMin"){
                // set state to pressable
                _pressable = 1;

                var selectionRenderer = selection.GetComponent<Renderer>();

                if (selectionRenderer != null){
                    // save the previous material to put back on later
                    _historicalMaterial = selectionRenderer.material;
                    // set current material to highlight material
                    selectionRenderer.material = HighlightMaterial;
                }
            }
            else{
                _pressable = 0;
            }
            _selection = selection;
        }
        else{
            _pressable = 0;
        }
    }


    public string DoAgentObjectAction(int agentAction){
        
        var ray = AgentCamera.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, AgentCamera.nearClipPlane));
        
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit)){
            var selection = hit.transform;

            if (selection.tag == "Selectable" || selection.tag == "SelectableMin" || selection.tag == "SelectablePlus"){
                Destroy(hit.transform.gameObject);

                if (selection.tag == "Selectable" && agentAction == 1){ 
                    // Debug.Log("correct choice");
                    return "correct";
                }
                if (selection.tag == "SelectablePlus" && agentAction == 2){ 
                    // Debug.Log("correct choice");
                    return "correct";
                }
                if (selection.tag == "SelectableMin" && agentAction == 3){ 
                    // Debug.Log("correct choice");
                    return "correct";
                }
                return "semiCorrect";
            }
            // Debug.Log($"wrong choice of {agentAction}. The tag was {selection.tag}");
        }
        return "incorrect";
    }
}
