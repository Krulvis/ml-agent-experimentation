using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using Random = UnityEngine.Random;


public class ButtonVisualAgent : Agent
{
    EnvironmentParameters m_ResetParams;
    public GameObject Environment;
    public GameObject SelectionManager;
    private SelectionManager _selectionManager;
    private EnvironmentController _environmentController;
    
    private int _XAxis = 0;
    private int _YAxis = 0;

    public override void Initialize()
    {
        // m_ResetParams = Academy.Instance.EnvironmentParameters;
    }


    public override void OnEpisodeBegin()
    {
        gameObject.transform.rotation = new Quaternion(0,0,0,0);
        // curriculum = (int)m_ResetParams.GetWithDefault("obstacles", curriculumStart);
        Debug.Log("episode began");

        _environmentController = Environment.GetComponent<EnvironmentController>();
        _selectionManager = SelectionManager.GetComponent<SelectionManager>();
        // reset the environment
        _environmentController.ResetEpisode();
        _environmentController.GenerateEpisode();
    }


    public override void CollectObservations(VectorSensor sensor)
    {
        // here its previous actions are set as input
        // it is one-hot encoded
        sensor.AddObservation(transform.forward.x);
        sensor.AddObservation(transform.forward.y);
        sensor.AddObservation(transform.forward.z);
        // Debug.Log($" x {transform.forward.x}");
        // Debug.Log($" z {transform.forward.z}");
        // Debug.Log($" y {transform.forward.y}");

        sensor.AddOneHotObservation(_YAxis, 3);
        sensor.AddOneHotObservation(_XAxis, 3);

        // set state for pressable. 0 is not pressable
        sensor.AddOneHotObservation(_selectionManager._pressable, 2);
    }


    private void Update() {
        RequestDecision();
    }

    /// <summary>
    /// Moves the agent according to the selected action.
    /// </summary>
    public void MoveAgent(ActionSegment<int> act)
    {
        _XAxis = act[1];
        _YAxis = act[0];

        // when 0, nothing is done

        Vector3 orientdir = new Vector3(0,0,0);

        switch (_YAxis)
        {
            case 1:
                orientdir += new Vector3(0,-1f,0);
                break;
            case 2:
                orientdir += new Vector3(0,1f,0);
                break;
        }

        switch (_XAxis)
        {
            case 1:
                orientdir += new Vector3(1f,0,0);
                break;
            case 2:
                orientdir += new Vector3(-1f,0,0);
                break;
        }

        var v3 = new Vector3(orientdir[0], orientdir[1], 0.0f);
        transform.Rotate(v3 * 100.0f * Time.deltaTime); 

        Quaternion q = transform.rotation;
        q.eulerAngles = new Vector3(q.eulerAngles.x, q.eulerAngles.y, 0);
        transform.rotation = q;
    }



    public override void WriteDiscreteActionMask(IDiscreteActionMask actionMask)
    {
        actionMask.SetActionEnabled(2, 1, isEnabled:  _selectionManager._pressable == 1 ? true : false);
    }


    /// <summary>
    /// Called every step of the engine. Here the agent takes an action.
    /// </summary>
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Move the agent using the action.
        MoveAgent(actionBuffers.DiscreteActions);

        // Do interaction action
        int agentInteraction = actionBuffers.DiscreteActions[2];
        if (agentInteraction > 0){

            string feedback = _selectionManager.DoAgentObjectAction(agentInteraction);
            // Debug.Log($"agent selection success is {success}");

            // in case of success, add reward, and generate new object
            if (feedback == "correct"){
                // Debug.Log("succes! 1f reward");
                AddReward(1f);
                _environmentController.SetTarget();
            }
            if (feedback == "semiCorrect"){
                AddReward(0.1f);
                _environmentController.SetTarget();
            }
        }

        // Penalty given each step to encourage agent to finish task quickly.
        // AddReward(-1f / MaxStep);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        /// three actions per direction. 0 is don't move. 1 is left/down, 2 is right/up
        var discreteActionsOut = actionsOut.DiscreteActions;
        
        // for camera orientation
        if (Input.GetKey(KeyCode.A))
        {
            discreteActionsOut[0] = 1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            discreteActionsOut[0] = 2;

        }
        if (Input.GetKey(KeyCode.S))
        {
            discreteActionsOut[1] = 1;
        }
        if (Input.GetKey(KeyCode.W))
        {
            discreteActionsOut[1] = 2;
        }

        // for red target interaction
        if (Input.GetKey(KeyCode.J))
        {
            discreteActionsOut[2] = 1;
        }
        // for plus target interaction
        if (Input.GetKey(KeyCode.K))
        {
            discreteActionsOut[2] = 2;
        }
        // for min target interaction
        if (Input.GetKey(KeyCode.L))
        {
            discreteActionsOut[2] = 3;
        }
    }
}
