using System;
using System.Collections.Generic;
using TMPro;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;
using UnityEngine.UI;

public class AgentScript : Agent
{
    public override void CollectObservations(VectorSensor sensor)
    {
        var challenge = GetComponent<Challenge>();
        sensor.AddObservation(challenge.currentValue);
        sensor.AddObservation(challenge.targetValue);
        var clicks = new List<float>(new float[5])
        {
            [Math.Min(4, challenge.CurrentClicks())] = 1
        };
        sensor.AddObservation(clicks);
    }

    public override void OnActionReceived(ActionBuffers buffer)
    {
        var buttonIndex = buffer.DiscreteActions[0];
        var button = GetComponent<Challenge>().buttonsRoot.transform.GetChild(buttonIndex).GetComponent<Button>();
        Debug.Log($"Pressing button: {button} with text: {button.GetComponentInChildren<TextMeshProUGUI>().text}");
        button.onClick.Invoke();
        AddReward(-1f / MaxStep);
        checkScores();
    }

    // Update is called once per frame
    void Update()
    {
        RequestDecision();
    }

    void checkScores()
    {
        var challenge = GetComponent<Challenge>();
        var currentClicks = challenge.CurrentClicks();
        if (currentClicks >= challenge.maxClicks)
        {
            Debug.Log($"Took too long `More than {challenge.maxClicks} tries` {currentClicks}");
            AddReward(-1);
            EndEpisode();
            return;
        }

        if (challenge.currentValue == challenge.targetValue)
        {
            Debug.Log("Found correct combination of buttons!");
            AddReward(1);
        }
        else if (challenge.currentValue > challenge.targetValue)
        {
            Debug.Log("Went too far!");
            AddReward(-1);
            EndEpisode();
        }
    }

    /// <summary>
    /// In the editor, if "Reset On Done" is checked then AgentReset() will be
    /// called automatically anytime we mark done = true in an agent script.
    /// </summary>
    public override void OnEpisodeBegin()
    {
        GetComponent<Challenge>().Reset();
    }
}