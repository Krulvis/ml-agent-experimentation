using System;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Challenge : MonoBehaviour
{
    public int targetValue = 8;
    public int currentValue = 0;
    public GameObject currentScoreTracker;

    public GameObject targetValueTracker;

    public GameObject buttonsRoot;

    public delegate void OnValueChanged(int newValue);

    public static OnValueChanged onValueChanged;

    private int clickCounter;
    public int maxClicks = 5;

    private void Awake()
    {
        onValueChanged += value => currentValue = value;
        onValueChanged += value => clickCounter++;
        onValueChanged += value =>
            currentScoreTracker.GetComponentInChildren<TextMeshProUGUI>().text = $"Score: {value.ToString()}";
    }

    /**
     * Fires the onValueChanged event
     */
    public void UpdateCurrentValue(int value)
    {
        onValueChanged(value + currentValue);
    }

    // Start is called before the first frame update
    void Start()
    {
        //Subscribe all button clicks to UpdateCurrentValue
        for (int i = 0; i < buttonsRoot.transform.childCount; i++)
        {
            var button = buttonsRoot.transform.GetChild(i).gameObject;
            var text = button.GetComponentInChildren<TextMeshProUGUI>();
            button.GetComponent<Button>().onClick.AddListener(() => UpdateCurrentValue(Int32.Parse(text.text)));
        }
    }

    public void Reset()
    {
        targetValue = Random.Range(1, 11);
        targetValueTracker.GetComponentInChildren<TextMeshProUGUI>().text = $"Target: {targetValue.ToString()}";
        onValueChanged(0);
        clickCounter = 0;
    }

    public int CurrentClicks()
    {
        return clickCounter;
    }
}